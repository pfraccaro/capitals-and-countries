﻿using Capitals_and_Countries.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Capitals_and_Countries
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        public GameWindow(BaseViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void AnswerButton_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as GameWindowViewModel).OnAnswerButtonClicked(sender as Button);
        }
    }
}
