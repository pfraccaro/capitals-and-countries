﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Capitals_and_Countries.ViewModel
{
    class MainMenuViewModel: BaseViewModel
    {
        private int _numberOfQuestions;
        public int NumberOfQuestions {
            get
            {
                return _numberOfQuestions;
            }
            set
            {
                _numberOfQuestions = value;
                OnPropertyChanged();
            }
        }
        #region Contructors
        public MainMenuViewModel()
        {
            NumberOfQuestions = 3;
        }
        #endregion Constructors

        #region Actions
        public ICommand ExitApplication { get { return new RelayCommand(ExitApplicationExecute, CanExitApplication); } }
        public ICommand PlayGame { get { return new RelayCommand(PlayGameExecute, CanPlayGame); } }
        private void PlayGameExecute()
        {
            GameWindowViewModel gameWindowViewModel = new GameWindowViewModel(NumberOfQuestions);
            var gameWindow = new GameWindow(gameWindowViewModel);
            gameWindow.Show();
        }

        private bool CanPlayGame()
        {
            return NumberOfQuestions <= 0 ? false : true;
        }

        private void ExitApplicationExecute()
        {
            Application.Current.Shutdown();
        }

        private bool CanExitApplication()
        {
            return true;
        }
        #endregion
    }
}
