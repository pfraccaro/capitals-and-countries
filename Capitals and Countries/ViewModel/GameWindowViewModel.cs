﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Capitals_and_Countries.Model;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Media;
using System.Timers;
using System.Threading;
using System.ComponentModel;

namespace Capitals_and_Countries.ViewModel
{
    class GameWindowViewModel: BaseViewModel
    {
        private Country _countryToGuess;
        public Country CountryToGuess { get { return _countryToGuess; } set {_countryToGuess = value; OnPropertyChanged(); } }

        private string _countryToGuessFlagPath;
        public string CountryToGuessFlagPath { get { return _countryToGuessFlagPath; } set { _countryToGuessFlagPath = value; OnPropertyChanged(); } }

        private List<Answer> _answersList = new List<Answer>();
        public List<Answer> AnswersList { get { return _answersList;} set { _answersList = value; OnPropertyChanged(); } }

        private List<Brush> _answerButtonsColors = new List<Brush>() { Brushes.LightGray, Brushes.LightGray, Brushes.LightGray, Brushes.LightGray };
        public List<Brush> AnswerButtonsColors { get { return _answerButtonsColors; }  set { _answerButtonsColors = value; OnPropertyChanged(); } }

        private int CorrectAnswerIndex;

        private List<Country> Countries;

        private int NumberOfQuestions;

        private int Score = 0;

        Random rand = new Random();
        public GameWindowViewModel(int numberOfQuestions)
        {
            string csvFilePath = @"C:\Users\Piotr\Desktop\Projekty\WPF\Capitals and Countries\Build 2\Capitals and Countries\Capitals and Countries\Assets\countries.csv";
            using (var streamReader = File.OpenText(csvFilePath))
            {
                NumberOfQuestions = numberOfQuestions;

                //Setup csv reader
                CsvReader reader = new CsvReader(streamReader);
                reader.Configuration.Delimiter = ";";

                //Fill Countries List with CSV countries
                Countries = reader.GetRecords<Country>().ToList();
            }

            //Ask first question
            AskNewQuestion();
        }

        public void AskNewQuestion()
        {
            if(NumberOfQuestions <= 0)
            {
                FinishGame();
                return;
            }

            //Generate 4 random indexes
            List<int> randomIndexes = GetManyUniqueRandomIntegers(4, 0, Countries.Count - 1);

            //Setup answers
            List<Answer> answers = new List<Answer>();
            foreach (int index in randomIndexes)
            {
                answers.Add(new Answer(Countries[index].Capital, false));
            }

            //It's devided to another variable because we don't want to call OnPropertyChanged 4 times ;)
            AnswersList = answers;

            //Set one answer to correct
            CorrectAnswerIndex = rand.Next(0, 3);
            AnswersList[CorrectAnswerIndex].IsCorrect = true;
            CountryToGuess = Countries[randomIndexes[CorrectAnswerIndex]];

            //Set image to correct flag
            CountryToGuessFlagPath = @"C:\Users\Piotr\Desktop\Projekty\WPF\Capitals and Countries\Build 2\Capitals and Countries\Capitals and Countries\Assets\Flags\%CountryCode%.png".Replace("%CountryCode%", CountryToGuess.CountryCode);

            //Decrement number of questions
            NumberOfQuestions--;
        }

        public void OnAnswerButtonClicked(Button sender)
        {
            //Get clicked answer
            int clickedAnswerIndex = Convert.ToInt32(sender.Tag);

            //Check if clicked answer is correct
            if(AnswersList[clickedAnswerIndex].IsCorrect)
            {
                Score++;
            }

            //Declare variables
            Answer answer;
            List<Brush> buttonColors = new List<Brush>();
               
            //Set answer buttons colors
            for (int i = 0; i < 4; i++)
            {
                answer = AnswersList[i];
                if (answer.IsCorrect)
                {
                    buttonColors.Insert(i, Brushes.Green);
                }
                else
                {
                    buttonColors.Insert(i, Brushes.Red);
                }
            }

            //It's devided to another variable because we don't want to call OnPropertyChanged 4 times ;)
            AnswerButtonsColors = buttonColors;

            //Pause thread to let user show result and ask another question
            var backgroundWorker = new BackgroundWorker();

            backgroundWorker.DoWork += (s, e) => 
            {
                Thread.Sleep(1200);
            };

            backgroundWorker.RunWorkerCompleted += (s, e) =>
            {
                ResetButtonsColors();
                AskNewQuestion();
            };

            backgroundWorker.RunWorkerAsync();
        }

        public void ResetButtonsColors()
        {
            AnswerButtonsColors = new List<Brush>() { Brushes.LightGray, Brushes.LightGray, Brushes.LightGray, Brushes.LightGray };
        }

        public void FinishGame()
        {

        }

        public List<int> GetManyUniqueRandomIntegers(int randomNumbersQuantity, int minimum, int maximum) {
            List<int> result = new List<int>();
            HashSet<int> check = new HashSet<int>();
            for (int i = 0; i < randomNumbersQuantity; i++)
            {
                int curValue = rand.Next(minimum, maximum);
                while (check.Contains(curValue))
                {
                    curValue = rand.Next(minimum, maximum);
                }
                result.Add(curValue);
                check.Add(curValue);
            }

            return result;
        }
    }
}
