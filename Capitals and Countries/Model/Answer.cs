﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capitals_and_Countries.Model
{
    class Answer
    {
        public string Capital { get; set; }
        public bool IsCorrect { get; set; }

        public Answer(string capital, bool isCorrect)
        {
            Capital = capital;
            IsCorrect = isCorrect;
        }
    }
}
