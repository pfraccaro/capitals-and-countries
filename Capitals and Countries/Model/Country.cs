﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capitals_and_Countries.Model
{
    class Country
    {
        public int CSVIndex { get; set; }
        public string NameCommon { get; set; }
        public string Capital { get; set; }
        public string CountryCode { get; set; }
    }
}
